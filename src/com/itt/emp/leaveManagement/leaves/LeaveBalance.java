package com.itt.emp.leaveManagement.leaves;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import com.itt.emp.leaveManagement.dao.BasicDaoOperations;
import com.itt.emp.leaveManagement.dao.ConnectionManager;
import com.itt.emp.leaveManagement.view.Communicator;

/**
 * This class is responsible for managing the leave and it's Data Members
 *
 * @author mayank.jain
 *
 */

public class LeaveBalance {
	private int employeeId;
	private int leaveId;
	private Date leaveFrom;
	private Date leaveTill;
	private String leaveStatus;
	private String reason;
	private String leaveType;

	BasicDaoOperations dao = new BasicDaoOperations();
	Communicator com = new Communicator();
	private static final String deniedViewLeaveBalance = "Leave Balance was not retrieved. Please Recheck the details.";
	private static final String deniedViewLeaveHistory = "Leave History couldn't be viewed. Please Recheck the details.";

	// SETTERS
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}

	public void setLeaveFrom(Date leaveFrom) {
		this.leaveFrom = leaveFrom;
	}

	public void setLeaveTill(Date leaveTill) {
		this.leaveTill = leaveTill;
	}

	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	// GETTERS
	public int getEmployeeId() {
		return employeeId;
	}

	public int getLeaveId() {
		return leaveId;
	}

	public Date getLeaveFrom() {
		return leaveFrom;
	}

	public Date getLeaveTill() {
		return leaveTill;
	}

	public String getLeaveStatus() {
		return leaveStatus;
	}

	public String getReason() {
		return reason;
	}

	public String getLeaveType() {
		return leaveType;
	}

	@Override
	public String toString() {
		return "LEAVEID :" + this.leaveId + " LEAVE_FROM :" + this.leaveFrom + " LEAVE TILL :" + this.leaveTill
				+ " ACTION :" + this.leaveStatus + " REASON :" + this.reason + " LEAVE TYPE : " + this.leaveType;
	}

	

	public void viewLeaveBalance(int employee_id) {
		int leavesApproved;
		try {
			leavesApproved = dao.viewLeaveBalanceDao(employee_id);
			System.out.println("Leaves Left: " + (25 - leavesApproved));
		} catch (SQLException e) {
			System.out.println(deniedViewLeaveBalance);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayEmployeeOptions(employee_id);
	}

	public void viewLeaveHistory(int employeeId) {
		try {
			List<LeaveBalance> leaveDetails = dao.viewLeaveHistoryDao(employeeId);
			for (int i = 0; i < leaveDetails.size(); i++) {
				System.out.println(leaveDetails.get(i));
			}
		} catch (SQLException e) {
			System.out.println(deniedViewLeaveHistory);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayEmployeeOptions(employeeId);
	}
}
