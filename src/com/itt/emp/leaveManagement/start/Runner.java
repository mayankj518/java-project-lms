package com.itt.emp.leaveManagement.start;

import java.sql.SQLException;

import com.itt.emp.leaveManagement.view.Communicator;

/**
 * This is the activator for the leave management system bundle
 *
 * @author mayank.jain
 *
 */

public class Runner {

	public static void main(String[] args) throws SQLException {
		Communicator com = new Communicator();
		System.out.println(" ***  WELCOME TO LEAVE MANAGEMENT SYSTEM  *** ");
		System.out.println();
		com.takeLoginDetails();
	}
}
