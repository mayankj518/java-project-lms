package com.itt.emp.leaveManagement.manager;

/**
 * This Interface will be implemented by EmailNotifaction and SMSNotification
 *
 * @author mayank.jain
 *
 */

public interface INotification {
	void sendNotification(String receiverId, String msg, int modeOfNotification);
}
