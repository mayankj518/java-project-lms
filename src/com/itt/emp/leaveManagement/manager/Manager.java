package com.itt.emp.leaveManagement.manager;

import java.sql.SQLException;
import java.util.List;

import com.itt.emp.leaveManagement.authentication.CredentialsManager;
import com.itt.emp.leaveManagement.dao.BasicDaoOperations;
import com.itt.emp.leaveManagement.dao.ConnectionManager;
import com.itt.emp.leaveManagement.leaves.LeaveBalance;
import com.itt.emp.leaveManagement.view.Communicator;

/**
 * This class manages the methods related to Manager
 *
 * @author mayank.jain
 *
 */

public final class Manager implements IManager {
	private int managerId;
	private String manager_name;
	private String email_id;
	private Long phone_no;
	private String department = "Management";
	private String role = "Manager";

	static CredentialsManager credManager = new CredentialsManager();
	BasicDaoOperations dao = new BasicDaoOperations();
	Communicator com = new Communicator();
	Notification notify = new Notification();

	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}

	public void viewLeaveHistory() {
		try {
			int employee_id = com.takeEmployeeId();
			boolean idExists = dao.checkExistanceDao(employee_id);
			if (idExists) {
				int count = 0;
				List<LeaveBalance> leaveDetails = dao.viewLeaveHistoryDao(employee_id);
				for (int i = 0; i < leaveDetails.size(); i++) {
					System.out.println(leaveDetails.get(i));
					count++;
				}
				if (count == 0)
					System.out.println(ManagerConstants.noLeaveHistory);
			} else
				System.out.println(ManagerConstants.idDoesNotExist);
		} catch (SQLException e) {
			System.out.println(ManagerConstants.deniedViewLeaveHistory);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayManagerOptions(managerId);
	}

	public void addEmployee(int employee_id, String employee_name, String email_id, long phone_no, String department,
			String username, String password) {
		try {
			boolean idExists = dao.checkExistanceDao(employee_id);
			if (!idExists) {
				int updatedEmployeeRows = dao.addEmployeeDao(employee_id, employee_name, email_id, phone_no,
						department);
				System.out.println("Rows Updated : " + updatedEmployeeRows);
				int updatedLoginRows = credManager.setCredentials(employee_id, username, password);
				if (updatedLoginRows == 1) {
					System.out.println(ManagerConstants.approvedAddingEmployee);
					String email = dao.getEmailIdDao(employee_id);
					String credentials = "USERNAME: " + username + "\nPASSWORD: " + password;
					notify.sendNotification(email, credentials, 1);
				}
			} else
				System.out.println(ManagerConstants.idAlreadyExist);
		} catch (SQLException e) {
			System.out.println(ManagerConstants.deniedAddingEmployee);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayManagerOptions(managerId);
	}

	public void removeEmployee() {
		try {
			int employee_id = com.takeEmployeeId();
			boolean idExists = dao.checkExistanceDao(employee_id);
			if (idExists) {
				int updatedLoginRows = credManager.removeCredentials(employee_id);
				int updatedEmployeeRows = dao.removeEmployeeDao(employee_id);
				if (updatedEmployeeRows == 1 && updatedLoginRows == 1)
					System.out.println(ManagerConstants.approvedRemovingEmployee);
				System.out.println();
			} else
				System.out.println(ManagerConstants.idDoesNotExist);
		} catch (SQLException e) {
			System.out.println(ManagerConstants.deniedRemovingEmployee);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayManagerOptions(managerId);
	}

	public void approveLeave() {
		try {
			int employee_id = com.takeEmployeeId();
			boolean leaveExists = dao.checkLeaveDao(employee_id);
			if (leaveExists) {
				List<LeaveBalance> leave = dao.viewLeaves(employee_id);
				for (int i = 0; i < leave.size(); i++) {
					System.out.println(leave.get(i));
				}
				System.out.println();
				int leave_id = com.takeLeaveId();
				String approvedLeave = dao.getLeaveDao(employee_id, leave_id);
				boolean leaveApproved = dao.approveLeaveDao(employee_id, leave_id);
				if (leaveApproved) {
					System.out.println(ManagerConstants.approvedApproveLeave);
					approvedLeave = "APPROVED LEAVE: " + approvedLeave;
					System.out.println(approvedLeave);
					String email = dao.getEmailIdDao(employee_id);
					notify.sendNotification(email, approvedLeave, 1);
				} else
					System.out.println(ManagerConstants.deniedApproveLeave);
			} else
				System.out.println(ManagerConstants.requestDoesNotExist);
		} catch (SQLException e) {
			System.out.println(ManagerConstants.deniedApproveLeave);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayManagerOptions(managerId);
	}

	public void denyLeave() {
		try {
			int employee_id = com.takeEmployeeId();
			boolean leaveExists = dao.checkLeaveDao(employee_id);
			if (leaveExists) {
				List<LeaveBalance> leave = dao.viewLeaves(employee_id);
				for (int i = 0; i < leave.size(); i++) {
					System.out.println(leave.get(i));
				}
				System.out.println();
				int leave_id = com.takeLeaveId();
				String deniedLeave = dao.getLeaveDao(employee_id, leave_id);
				int updatedRows = dao.denyLeaveDao(employee_id, leave_id);
				if (updatedRows == 1) {
					System.out.println(ManagerConstants.approvedDenyLeave);
					deniedLeave = "DENIED LEAVE: " + deniedLeave;
					System.out.println(deniedLeave);
					String email = dao.getEmailIdDao(employee_id);
					notify.sendNotification(email, deniedLeave, 1);

				} else
					System.out.println(ManagerConstants.deniedDenyLeave);
			} else
				System.out.println(ManagerConstants.requestDoesNotExist);
		} catch (SQLException e) {
			System.out.println(ManagerConstants.deniedDenyLeave);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayManagerOptions(managerId);
	}

	public void viewLeaveRequest() {
		try {
			List<LeaveBalance> leave = dao.viewLeaveRequestDao();
			for (int i = 0; i < leave.size(); i++) {
				System.out.println(leave.get(i));
			}

		} catch (SQLException e) {
			System.out.println(ManagerConstants.deniedViewLeaveRequest);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayManagerOptions(managerId);
	}

}
