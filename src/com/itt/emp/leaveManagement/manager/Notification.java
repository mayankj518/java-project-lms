package com.itt.emp.leaveManagement.manager;

/**
 * @author MAYANK
 *
 */
public class Notification implements INotification {
	NotificationEmail notifyEmail = new NotificationEmail();

	public void sendNotification(String receiverID, String msg, int modeOfNotification) {
		switch (modeOfNotification) {
		case 1:
			notifyEmail.sendEmail(receiverID, msg);
			break;
		case 2:
			System.out.println("SMS Mode of Notification is currently under progress.");
		}
	}
}
