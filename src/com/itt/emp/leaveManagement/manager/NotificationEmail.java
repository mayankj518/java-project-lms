package com.itt.emp.leaveManagement.manager;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * This class is responsible for all the Email Notifications
 *
 * @author mayank.jain
 *
 */

public class NotificationEmail {

	void sendEmail(String to, String msg) {
		if (msg.contains("APPROVED"))
			approveLeaveEmail(to, msg);
		else if (msg.contains("DENIED"))
			denyLeaveEmail(to, msg);
		else if (msg.contains("USERNAME"))
			sendCredentialsEmail(to, msg);
	}

	private void approveLeaveEmail(String to, String leave) {
		try {
			MimeMessage message = setEmailDetails(to);
			message.setSubject("Leave Request Approved");
			message.setText("Your Leave Request has been approved: \n " + leave);
			Transport.send(message);
			System.out.println("Mail sent successfully");
		} catch (MessagingException mex) {
			System.out.println("Approved Leave couldn't be send through mail.");
		}
	}

	private void denyLeaveEmail(String to, String leave) {
		try {
			MimeMessage message = setEmailDetails(to);
			message.setSubject("Leave Request Denied");
			message.setText("Your Leave Request has been Denied: \n " + leave);
			Transport.send(message);
			System.out.println("Mail sent successfully");
		} catch (MessagingException mex) {
			System.out.println("Denied Leave couldn't be send through mail.");
		}

	}

	private void sendCredentialsEmail(String to, String credentials) {
		try {
			MimeMessage message = setEmailDetails(to);
			message.setSubject("ITT Login Credentials");
			message.setText("Welcome to ITT \n Your Login Credentials are : \n" + credentials);
			Transport.send(message);
			System.out.println("Mail sent successfully");
		} catch (MessagingException mex) {
			System.out.println("Login Credentials couldn't be send through mail. Please check email id once again.");
		}

	}

	private static MimeMessage setEmailDetails(String to) throws MessagingException, SendFailedException {
		String from = "mayankj518@gmail.com";

		Properties properties = createConfiguration();

		SmtpAuthenticator authentication = new SmtpAuthenticator();

		Session session = Session.getDefaultInstance(properties, authentication);
		MimeMessage message = new MimeMessage(session);

		message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		return message;
	}

	private static Properties createConfiguration() {
		return new Properties() {
			{
				put("mail.smtp.auth", "true");
				put("mail.smtp.host", "smtp.gmail.com");
				put("mail.smtp.port", "587");
				put("mail.smtp.starttls.enable", "true");
			}
		};
	}

	private static class SmtpAuthenticator extends Authenticator {

		private String username = "mayankj518@gmail.com";
		private String password = "jain.mayank98";

		@Override
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	}

}
