package com.itt.emp.leaveManagement.manager;

/**
 * This Interface will be implemented by Manager
 *
 * @author mayank.jain
 *
 */

public interface IManager {
	void addEmployee(int employee_id,String employee_name,String email_id,long phone_no,String department,String username,String password);
	void removeEmployee();
	void approveLeave();
	void denyLeave();
	void viewLeaveRequest();
	
}
