package com.itt.emp.leaveManagement.manager;

public class ManagerConstants {
	static final String deniedAddingEmployee = "Employee wasn't added. Recheck your details.";
	static final String approvedAddingEmployee = "Employee has been added. Credentials have been added.";
	static final String deniedRemovingEmployee = "Employee wasn't removed. Please Check the Employee ID";
	static final String approvedRemovingEmployee = "Employee has been removed. Credentails have been removed.";
	static final String deniedDenyLeave = "Leave Request wasn't Denied.";
	static final String approvedDenyLeave = "Leave Request Denied.";
	static final String deniedApproveLeave = "Leave Request wasn't Approved.";
	static final String approvedApproveLeave = "Leave Request Approved.";
	static final String deniedViewLeaveRequest = "Leave Requests couldn't be viewed.";
	static final String deniedViewLeaveHistory = "Leave History can't be viewed. Please check the Details.";
	static final String noLeaveHistory = "This Employee has has no Leave History.";
	static final String idDoesNotExist = "Employee ID doesn't Exist";
	static final String idAlreadyExist = "Employee ID Already Exists.";
	static final String requestDoesNotExist = "Leave Request Doesn't Exist";
}
