package com.itt.emp.leaveManagement.employee;

/**
 * This Interface will be implemented by Employee
 *
 * @author mayank.jain
 *
 */

public interface IEmployee {
	void applyForLeave();
	void viewPendingLeaves();
}
