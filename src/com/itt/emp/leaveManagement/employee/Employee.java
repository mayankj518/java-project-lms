package com.itt.emp.leaveManagement.employee;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import com.itt.emp.leaveManagement.authentication.CredentialsManager;
import com.itt.emp.leaveManagement.dao.BasicDaoOperations;
import com.itt.emp.leaveManagement.dao.ConnectionManager;
import com.itt.emp.leaveManagement.leaves.LeaveBalance;
import com.itt.emp.leaveManagement.view.Communicator;

/**
 * This class manages the methods related to Employee
 *
 * @author mayank.jain
 *
 */

public class Employee extends LeaveBalance implements IEmployee {
	private int employeeId;
	private String employeeName;
	private String emailId;
	private Long phoneNo;
	private String department;
	private String role = "Employee";

	BasicDaoOperations dao = new BasicDaoOperations();
	Communicator com = new Communicator();
	CredentialsManager credManager = new CredentialsManager();

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	private static final String leaveRequestSubmitted = "Leave Request Submitted";
	private static final String deniedViewPendingLeaves = "Pending Leaves couldn't be viewed. Please check the details.";
	private static final String wrongCredentialsReset = "Wrong Original Username or Password for Reset. Kindly try again with the correct Credentials.";
	private static final String wrongDateInput = "Problem in taking Date Input. Please Check the details";

	// Overriding for Printing the List which was used to map ResultSet to Java
	// Object
	@Override
	public String toString() {
		System.out.println();
		return "LEAVEID :" + this.getLeaveId() + " LEAVE FROM :" + this.getLeaveFrom() + " LEAVE TILL :"
				+ this.getLeaveTill() + " ACTION :" + this.getLeaveStatus() + " REASON :" + this.getReason()
				+ " LEAVE TYPE : " + this.getLeaveType();
	}

	public void applyForLeave() {
		try {
			String[] leaveDetails = com.takeDateDetails();
			int id = employeeId;
			boolean leaveSubmitted = dao.applyForLeaveDao(leaveDetails[0], leaveDetails[1], id, leaveDetails[2],
					leaveDetails[3]);
			if (leaveSubmitted)
				System.out.println(leaveRequestSubmitted);
			else
				System.out.println("Invalid Input Format");
		} catch (SQLException e) {
			System.out.println("Invalid Input.");
		} catch (IllegalArgumentException e) {
			System.out.println(wrongDateInput);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayEmployeeOptions(employeeId);

	}

	public void viewPendingLeaves() {
		try {
			int id = this.employeeId;
			List<LeaveBalance> leaveDetails = dao.viewPendingLeavesDao(id);
			for (int i = 0; i < leaveDetails.size(); i++) {
				System.out.println(leaveDetails.get(i));
			}
			System.out.println();

		} catch (SQLException e) {
			System.out.println(deniedViewPendingLeaves);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayEmployeeOptions(employeeId);
	}

	public void resetCredentials() {
		try {
			credManager.resetCredentials();
		} catch (NullPointerException e) {
			System.out.println(wrongCredentialsReset);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
		com.displayEmployeeOptions(employeeId);

	}

}
