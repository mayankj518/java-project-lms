package com.itt.emp.leaveManagement.dao;

import java.sql.*;

/**
 * This class manages the connection between the Java Application and Database
 *
 * @author mayank.jain
 *
 */

public class ConnectionManager {
	static Connection con = null;

	public static Connection getConnection() throws SQLException {
		if (con != null && !con.isClosed())
			return con;
		return getConnection("lms", "root", "jain.mayank98");
	}

	private static Connection getConnection(String db_name, String user_name, String password) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + db_name, user_name, password);
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		}
		return con;
	}

	public static void closeConnection() {
		try {
			if (con != null)
				con.close();
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		}
	}
}
