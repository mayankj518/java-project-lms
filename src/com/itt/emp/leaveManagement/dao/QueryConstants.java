package com.itt.emp.leaveManagement.dao;

public class QueryConstants {
	static final String loginQuery = "select * from login where username = ?";
	static final String resetCredentialsQuery = "update login SET username= ?,password=? where username = ?";
	static final String getRoleQuery = "select * from employee where employee_id = ?";
	static final String checkLeaveQuery = "select * from leave_details where employee_id =? and leave_status='Pending'";
	static final String getEmailIdQuery = "select email_id from employee where employee_id =?";
	static final String getNameQuery = "select * from employee where employee_id = ?";
	static final String addEmployeeQuery = "insert into employee values(?,?,?,?,?,?)";
	static final String setCredentialsQuery = "insert into login values(?,?,?)";
	static final String removeCredentialsQuery = "delete from login where id = ?";
	static final String removeEmployeeQuery = "delete from employee where employee_id = ?";
	static final String approveLeaveQuery = "update leave_details SET leave_status='Approved' where employee_id = ? and leave_id = ? and leave_status='Pending'";
	static final String viewLeavesQuery = "select * from leave_details where employee_id = ?  and leave_status='Pending'";
	static final String denyLeaveQuery = "update leave_details SET leave_status='Denied' where employee_id = ? and leave_id = ? and leave_status = 'Pending'";
	static final String getLeaveQuery = "select * from leave_details where employee_id =? and leave_id = ?";
	static final String checkExistanceQuery = "select * from employee where employee_id =?";
	static final String viewLeaveRequestQuery = "select * from leave_details where leave_status='Pending'";
	static final String viewLeaveHistoryQuery = "select * from leave_details where employee_id = ? and NOT leave_status = 'Pending'";
	static final String viewPendingLeavesQuery = "select * from leave_details where employee_id = ? and leave_status ='Pending'";
	static final String viewLeaveBalanceQuery = "select COUNT(*) AS total from leave_details where employee_id = ? and leave_status = 'Approved'";
	static final String applyForLeaveQuery = "insert into leave_details (employee_id,leave_from,leave_till,Reason,leave_type) values (?,?,?,?,?)";

}
