package com.itt.emp.leaveManagement.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.itt.emp.leaveManagement.authentication.Authentication;
import com.itt.emp.leaveManagement.leaves.LeaveBalance;

/**
 * This class is responsible for all the DAO Operations s
 *
 * @author mayank.jain
 *
 */


public class BasicDaoOperations {

	public Authentication loginDao(String username) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.loginQuery);
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		Authentication auth = new Authentication();
		while (rs.next()) {
			auth.setId(rs.getInt(1));
			auth.setUsername(rs.getString(2));
			auth.setPassword(rs.getString(3));
		}

		return auth;
	}

	public boolean resetCredentialsDao(String username, String newUsername, String newPassword) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.resetCredentialsQuery);
		ps.setString(1, newUsername);
		ps.setString(2, newPassword);
		ps.setString(3, username);
		int updatedRows = ps.executeUpdate();
		boolean credentialsReseted = false;
		if (updatedRows == 1)
			credentialsReseted = true;
		return credentialsReseted;
	}

	public String getRoleDao(int id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.getRoleQuery);
		ps.setInt(1, id);
		ResultSet rs1 = ps.executeQuery();
		rs1.next();
		if (rs1.getString(6).equals("Employee"))
			return "Employee";
		return "Manager";

	}

	public boolean checkLeaveDao(int employee_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.checkLeaveQuery);
		ps.setInt(1, employee_id);
		ResultSet rs = ps.executeQuery();
		int count = 0;
		while (rs.next()) {
			count++;
		}
		boolean leaveExists = true;
		if (count == 0)
			leaveExists = false;
		return leaveExists;
	}

	public String getEmailIdDao(int employee_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.getEmailIdQuery);
		ps.setInt(1, employee_id);
		ResultSet rs = ps.executeQuery();
		rs.next();
		return rs.getString(1);
	}

	public String getNameDao(int id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.getNameQuery);
		ps.setInt(1, id);
		ResultSet rs1 = ps.executeQuery();
		rs1.next();
		return rs1.getString(2);
	}

	public int addEmployeeDao(int employee_id, String employee_name, String email_id, long phone_no, String department)
			throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.addEmployeeQuery);
		ps.setInt(1, employee_id);
		ps.setString(2, employee_name);
		ps.setString(3, email_id);
		ps.setLong(4, phone_no);
		ps.setString(5, department);
		ps.setString(6, "Employee");
		return ps.executeUpdate();
	}

	public int setCredentialsDao(int employee_id, String username, String password) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.setCredentialsQuery);
		ps.setInt(1, employee_id);
		ps.setString(2, username);
		ps.setString(3, password);
		return ps.executeUpdate();
	}

	public int removeCredentialsDao(int employee_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection()
				.prepareStatement(QueryConstants.removeCredentialsQuery);
		ps.setInt(1, employee_id);
		return ps.executeUpdate();
	}

	public int removeEmployeeDao(int employee_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.removeEmployeeQuery);
		ps.setInt(1, employee_id);
		return ps.executeUpdate();
	}

	public boolean approveLeaveDao(int employee_id, int leave_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.approveLeaveQuery);
		ps.setInt(1, employee_id);
		ps.setInt(2, leave_id);
		int updatedRows = ps.executeUpdate();
		boolean leaveApproved = false;
		if (updatedRows == 1)
			leaveApproved = true;
		return leaveApproved;

	}

	public List viewLeaves(int employee_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.viewLeavesQuery);
		ps.setInt(1, employee_id);
		ResultSet rs = ps.executeQuery();
		List<LeaveBalance> leave = new ArrayList<LeaveBalance>();
		while (rs.next()) {
			LeaveBalance l = new LeaveBalance();
			l.setEmployeeId(rs.getInt(2));
			l.setLeaveId(rs.getInt(1));
			l.setLeaveFrom(rs.getDate(3));
			l.setLeaveTill(rs.getDate(4));
			l.setLeaveStatus(rs.getString(5));
			l.setReason(rs.getString(6));
			l.setLeaveType(rs.getString(7));
			leave.add(l);
		}
		return leave;
	}

	public int denyLeaveDao(int employee_id, int leave_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.denyLeaveQuery);
		ps.setInt(1, employee_id);
		ps.setInt(2, leave_id);
		return ps.executeUpdate();
	}

	public String getLeaveDao(int employee_id, int leave_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.getLeaveQuery);
		ps.setInt(1, employee_id);
		ps.setInt(2, leave_id);
		ResultSet rs = ps.executeQuery();
		rs.next();
		String leave = "LEAVE ID: " + rs.getInt(1) + " EMPLOYEE ID: " + rs.getInt(2) + " LEAVE FROM: " + rs.getDate(3)
				+ " LEAVE TILL: " + rs.getDate(4) + " STATUS : " + rs.getString(5) + " REASON: " + rs.getString(6)
				+ " LEAVE TYPE: " + rs.getString(7);
		return leave;
	}

	public boolean checkExistanceDao(int employee_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.checkExistanceQuery);
		ps.setInt(1, employee_id);
		ResultSet rs = ps.executeQuery();
		int count = 0;
		while (rs.next()) {
			count++;
		}
		boolean idExists = false;
		if (count == 1)
			idExists = true;
		return idExists;
	}

	public List viewLeaveRequestDao() throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.viewLeaveRequestQuery);
		ResultSet rs = ps.executeQuery();
		List<LeaveBalance> leave = new ArrayList<LeaveBalance>();
		while (rs.next()) {
			LeaveBalance l = new LeaveBalance();
			l.setEmployeeId(rs.getInt(2));
			l.setLeaveId(rs.getInt(1));
			l.setLeaveFrom(rs.getDate(3));
			l.setLeaveTill(rs.getDate(4));
			l.setLeaveStatus(rs.getString(5));
			l.setReason(rs.getString(6));
			l.setLeaveType(rs.getString(7));
			leave.add(l);
		}
		return leave;
	}

	public List viewLeaveHistoryDao(int employee_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.viewLeaveHistoryQuery);
		ps.setInt(1, employee_id);
		ResultSet rs = ps.executeQuery();
		List<LeaveBalance> leave = new ArrayList<LeaveBalance>();
		while (rs.next()) {
			LeaveBalance l = new LeaveBalance();
			l.setEmployeeId(rs.getInt(2));
			l.setLeaveId(rs.getInt(1));
			l.setLeaveFrom(rs.getDate(3));
			l.setLeaveTill(rs.getDate(4));
			l.setLeaveStatus(rs.getString(5));
			l.setReason(rs.getString(6));
			l.setLeaveType(rs.getString(7));
			leave.add(l);
		}
		return leave;
	}

	public List viewPendingLeavesDao(int employee_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.viewPendingLeavesQuery);
		ps.setInt(1, employee_id);
		ResultSet rs = ps.executeQuery();
		List<LeaveBalance> leave = new ArrayList<LeaveBalance>();
		while (rs.next()) {
			LeaveBalance l = new LeaveBalance();
			l.setEmployeeId(rs.getInt(2));
			l.setLeaveId(rs.getInt(1));
			l.setLeaveFrom(rs.getDate(3));
			l.setLeaveTill(rs.getDate(4));
			l.setLeaveStatus(rs.getString(5));
			l.setReason(rs.getString(6));
			l.setLeaveType(rs.getString(7));
			leave.add(l);
		}
		return leave;
	}

	public int viewLeaveBalanceDao(int employee_id) throws SQLException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.viewLeaveBalanceQuery);
		ps.setInt(1, employee_id);
		ResultSet rs = ps.executeQuery();
		rs.next();
		int leavesApproved = rs.getInt("total");
		return leavesApproved;
	}

	public boolean applyForLeaveDao(String leave_from, String leave_till, int employee_id, String reason,
			String leave_type) throws SQLException, IllegalArgumentException {
		PreparedStatement ps = ConnectionManager.getConnection().prepareStatement(QueryConstants.applyForLeaveQuery);
		ps.setInt(1, employee_id);
		ps.setDate(2, Date.valueOf(leave_from));
		ps.setDate(3, Date.valueOf(leave_till));
		ps.setString(4, reason);
		ps.setString(5, leave_type);
		int updatedRows = ps.executeUpdate();
		boolean leaveApplied = false;
		if (updatedRows == 1)
			leaveApplied = true;
		return leaveApplied;

	}
}
