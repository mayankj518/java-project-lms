package com.itt.emp.leaveManagement.authentication;

import java.sql.SQLException;

import com.itt.emp.leaveManagement.dao.BasicDaoOperations;
import com.itt.emp.leaveManagement.view.Communicator;

/**
 * This class is responsible for managing the credentials
 *
 * @author mayank.jain
 *
 */

public class CredentialsManager {

	BasicDaoOperations dao = new BasicDaoOperations();
	Communicator com = new Communicator();

	private static final String updated = "Credentials have been updated.";
	private static final String notUpdated = "Credentials were not updated.";
	private static final String wrongCredentials = "Incorrect Username or Password for Reset. Kindly try again with the correct Credentials.";

	public int setCredentials(int employee_id, String username, String password) throws SQLException {
		return dao.setCredentialsDao(employee_id, username, password);
	}

	public int removeCredentials(int employee_id) throws SQLException {
		return dao.removeCredentialsDao(employee_id);
	}

	public void resetCredentials(){
		try {
			String[] credentials = com.takeResetCredentialsDetails();
			String username = credentials[0];
			String newUsername = credentials[1];
			String newPassword = credentials[2];
			boolean credentialsUpdated = dao.resetCredentialsDao(username, newUsername, newPassword);
			if (credentialsUpdated)
				System.out.println(updated);
			else
				System.out.println(notUpdated);
		} catch (SQLException e) {
			System.out.println(wrongCredentials);
		} catch (Exception e) {
			System.out.println(wrongCredentials);
		}
	}
}
