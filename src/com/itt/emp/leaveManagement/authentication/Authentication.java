package com.itt.emp.leaveManagement.authentication;

import java.sql.*;

import com.itt.emp.leaveManagement.dao.BasicDaoOperations;
import com.itt.emp.leaveManagement.dao.ConnectionManager;
import com.itt.emp.leaveManagement.view.Communicator;

/**
 * This class is responsible for logging in the user
 *
 * @author mayank.jain
 *
 */

public class Authentication {
	private int id;
	private String username;
	private String password;

	BasicDaoOperations dao = new BasicDaoOperations();
	Communicator com = new Communicator();

	public void setId(int id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public int getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void login(String username, String password) {
		try {
			Authentication loginDetails = dao.loginDao(username);
			if (loginDetails.password.equals(password)) {
				System.out.println("Login Successfull");
				int id = loginDetails.id;
				String role = dao.getRoleDao(id);
				String name = dao.getNameDao(id);
				System.out.println();
				System.out.println("WELCOME " + name.toUpperCase());
				if (role.equals("Employee"))
					com.displayEmployeeOptions(id);
				else
					com.displayManagerOptions(id);

			} else
				callIncorrectLogin();
		} catch (SQLException | NullPointerException e) {
			callIncorrectLogin();
		} catch (Exception e) {
			System.out.println("Something went wrong. Please Try Again Later");
			e.printStackTrace();
		} finally {
			ConnectionManager.closeConnection();
		}
	}

	private void callIncorrectLogin() {
		System.out.println("Incorrect username or password");
		System.out.println();
		com.takeLoginDetails();
	}

}
