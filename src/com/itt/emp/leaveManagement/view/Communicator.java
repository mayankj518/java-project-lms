package com.itt.emp.leaveManagement.view;

import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.itt.emp.leaveManagement.authentication.Authentication;
import com.itt.emp.leaveManagement.dao.BasicDaoOperations;
import com.itt.emp.leaveManagement.dao.ConnectionManager;
import com.itt.emp.leaveManagement.employee.Employee;
import com.itt.emp.leaveManagement.leaves.LeaveBalance;
import com.itt.emp.leaveManagement.manager.Manager;

/**
 * This class is responsible for taking input from the user
 *
 * @author mayank.jain
 *
 */

public class Communicator {
	static int input;
	static Scanner scan = new Scanner(System.in);
	static Manager m = new Manager();
	static Employee e = new Employee();
	static BasicDaoOperations dao = new BasicDaoOperations();
	static LeaveBalance leave = new LeaveBalance();

	public void takeEmployeeDetails() {
		int employeeId = 0;
		Long phoneNo = 0L;
		String employeeName = "", emailId = "";
		System.out.println("ENTER THE EMPLOYEE'S DETAILS:");
		System.out.print("EMPLOYEE'S ID: ");

		try {
			employeeId = scan.nextInt();
			scan.nextLine();
			System.out.print("EMPLOYEE'S NAME: ");
			employeeName = scan.nextLine();
			System.out.print("EMAIL ID: ");
			emailId = scan.nextLine();
			System.out.print("PHONE NO: ");
			phoneNo = scan.nextLong();
		} catch (InputMismatchException e) {
			System.out.println();
			System.out.println("Please Enter a valid Input");
			scan.nextLine();
			takeEmployeeDetails();
		}

		scan.nextLine();
		System.out.print("DEPARTMENT: ");
		String department = scan.nextLine();

		System.out.println();
		System.out.println("**** NOW SET THE CREDENTIALS FOR THE EMPLOYEE ****");
		System.out.print("USERNAME: ");
		String username = scan.nextLine();
		System.out.print("PASSWORD: ");
		String password = scan.nextLine();

		m.addEmployee(employeeId, employeeName, emailId, phoneNo, department, username, password);
	}

	public static void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
			System.exit(0);
	}

	private String[] takeFullDayLeaveDetails(String[] leaveDetails){
		System.out.println("ENTER THE DATE IN (YYYY-MM-DD) FORMAT");
		System.out.print("LEAVE FROM (DATE): ");
		leaveDetails[0] = takeStringInput();
		System.out.print("LEAVE TILL (DATE): ");
		leaveDetails[1] = takeStringInput();
		System.out.print("ENTER THE REASON: ");
		leaveDetails[2] = takeStringInput();
		leaveDetails[3] = "Full Day Leave";
		return leaveDetails;
	}

	private String[] takeHalfDayLeaveDetails(String[] leaveDetails) throws SQLException{
		System.out.print("\nPRESS 1 : 1st HALF\nPRESS 2 : 2nd HALF\nENTER FOR WHICH HALF YOU ARE TAKING THE LEAVE: ");
		String timeOfLeave = takeStringInput();
		if (timeOfLeave.equals("1"))
			leaveDetails[3] = "Half Day Leave (1st Half)";
		else if (timeOfLeave.equals("2")){
			leaveDetails[3] = "Half Day Leave (2nd Half)";
			System.out.println("ENTER THE DATE IN (YYYY-MM-DD) FORMAT");
			System.out.print("ENTER THE DATE: ");
			leaveDetails[0] = takeStringInput();
			leaveDetails[1] = leaveDetails[0];
			System.out.print("ENTER THE REASON: ");
			leaveDetails[2] = takeStringInput();
		}
		else throw new SQLException();
		return leaveDetails;
	}

	public String[] takeDateDetails() throws SQLException{
		String leaveDetails[] = new String[4];
		System.out.print("PRESS 0 : HALF DAY LEAVE \nPRESS 1 : FULL DAY LEAVE\nENTER THE LEAVE TYPE : ");
		String leaveType = scan.nextLine();
		if (leaveType.equals("1")) {
			leaveDetails = takeFullDayLeaveDetails(leaveDetails);
		} else if (leaveType.equals("0")) {
			leaveDetails = takeHalfDayLeaveDetails(leaveDetails);
		} else throw new SQLException();

		return leaveDetails;
	}

	public int takeEmployeeId() {
		System.out.print("ENTER THE EMPLOYEE'S ID: ");
		int employee_id = scan.nextInt();
		return employee_id;
	}

	public int takeLeaveId() {
		System.out.print("ENTER THE LEAVE ID: ");
		int leave_id = scan.nextInt();
		return leave_id;
	}

	public String takeStringInput(){
		String inputString = scan.nextLine();
		return inputString;

	}

	public String[] takeResetCredentialsDetails() throws SQLException {
		String[] resetCredentials = new String[3];
		System.out.print("ENTER THE CURRENT USERNAME: ");
		resetCredentials[0] = scan.nextLine();
		System.out.print("ENTER THE CURRENT PASSWORD: ");
		String password = scan.nextLine();
		dao.loginDao(resetCredentials[0]);

		Authentication auth = dao.loginDao(resetCredentials[0]);
		if (auth.getPassword().equals(password)) {
			System.out.print("ENTER THE NEW USERNAME: ");
			resetCredentials[1] = scan.nextLine();
			System.out.print("ENTER THE NEW PASSWORD: ");
			resetCredentials[2] = scan.nextLine();
		} 
		else throw new SQLException();
		return resetCredentials;

	}


	public void takeLoginDetails() {
		// Console console = System.console();
		System.out.println("ENTER YOUR LOGIN DETAILS: ");
		System.out.println();
		Scanner sc = new Scanner(System.in);
		System.out.print("ENTER YOUR USERNAME: ");
		String username = sc.nextLine();
		System.out.print("PASSWORD: ");
		String password = sc.nextLine();
		// String password = new String(console.readPassword("ENTER PASSWORD:
		// "));
		Authentication a = new Authentication();
		a.login(username, password);
		sc.close();
	}

	public void displayEmployeeOptions(int id) {
		System.out.println();
		System.out.println("PRESS 1 : APPLY FOR LEAVE");
		System.out.println("PRESS 2 : VIEW PENDING LEAVES");
		System.out.println("PRESS 3 : VIEW LEAVE HISTORY");
		System.out.println("PRESS 4 : VIEW LEAVE BALANCE");
		System.out.println("PRESS 5 : RESET CREDENTIALS");
		System.out.println("PRESS 6 : LOGOUT");
		System.out.println("PRESS 0 : TERMINATE THE APPLICATION");
		System.out.println();
		System.out.print("Enter Your Input : ");
		try {
			input = scan.nextInt();
			employeeOptions(input, id);
		} catch (InputMismatchException e) {
			System.out.println("Please Enter a valid input");
			scan.nextLine();
			displayEmployeeOptions(id);
		}
	}

	private void terminateProgram() {
		System.out.println(" ***** THANK YOU FOR USING LEAVE MANAGEMENT SYSTEM *****");
		System.exit(0);
	}

	public void displayManagerOptions(int id) {
		System.out.println();
		System.out.println("PRESS 1 : ADD EMPLOYEE");
		System.out.println("PRESS 2 : REMOVE EMPLOYEE");
		System.out.println("PRESS 3 : APPROVE LEAVE");
		System.out.println("PRESS 4 : DENY LEAVE");
		System.out.println("PRESS 5 : VIEW LEAVE REQUEST");
		System.out.println("PRESS 6 : VIEW LEAVE HISTORY");
		System.out.println("PRESS 7 : LOGOUT");
		System.out.println("PRESS 0 : TERMINATE THE APPLICATION");
		System.out.println();
		System.out.print("Enter Your Input : ");
		try {
			input = scan.nextInt();
			managerOptions(input, id);
		} catch (InputMismatchException e) {
			System.out.println("Please Enter a valid input");
			scan.nextLine();
			displayManagerOptions(id);
		}

	}

	private void managerOptions(int input, int id) {
		Manager m = new Manager();
		m.setManagerId(id);

		switch (input) {
		case 1:
			takeEmployeeDetails();
			break;
		case 2:
			m.removeEmployee();
			break;
		case 3:
			m.approveLeave();
			break;
		case 4:
			m.denyLeave();
			break;
		case 5:
			m.viewLeaveRequest();
			break;
		case 6:
			m.viewLeaveHistory();
			break;
		case 7:
			scan.nextLine();
			ConnectionManager.closeConnection();
			takeLoginDetails();
			break;
		case 0:
			terminateProgram();
		default:
			System.out.println("Please Enter a valid input");
			displayManagerOptions(id);
			break;
		}
	}

	private void employeeOptions(int input, int id) {
		Employee e = new Employee();
		e.setEmployeeId(id);

		switch (input) {
		case 1:
			scan.nextLine();
			e.applyForLeave();
			break;
		case 2:
			e.viewPendingLeaves();
			break;
		case 3:
			leave.viewLeaveHistory(id);
			break;
		case 4:
			e.viewLeaveBalance(id);
			break;
		case 5:
			scan.nextLine();
			e.resetCredentials();
		case 6:
			scan.nextLine();
			ConnectionManager.closeConnection();
			takeLoginDetails();
			break;
		case 0:
			terminateProgram();
		default:
			System.out.println("Please Enter a valid input");
			displayEmployeeOptions(id);
			break;
		}
	}

	private void takeOptionInput(int id) {
		System.out.print("Enter Your Input : ");
		try {
			input = scan.nextInt();
			employeeOptions(input, id);
		} catch (InputMismatchException e) {
			System.out.println("Please Enter a valid input");
		}
	}
}
